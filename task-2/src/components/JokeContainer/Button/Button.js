import React from 'react';
import './Button.css';

const Button = props => {
  return (
    <button onClick={props.onClick} className="Button">Get new joke</button>
  );
};

export default Button;
