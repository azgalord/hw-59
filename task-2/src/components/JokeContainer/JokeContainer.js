import React from 'react';
import Joke from "./Joke/Joke";
import Button from "./Button/Button";

const JokeContainer = props => {
  return (
    <div className="JokeContainer">
      <Joke
        img={props.img}
        text={props.text}
      />
      <Button
        onClick={() => props.onClick()}
      />
    </div>
  );
};

export default JokeContainer;
