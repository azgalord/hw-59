import React from 'react';
import './LogoContainer.css';

const LogoContainer = () => {
  return (
    <div>
      <img src="https://assets.chucknorris.host/img/chucknorris_logo_coloured_small.png" alt=""/>
    </div>
  );
};

export default LogoContainer;
