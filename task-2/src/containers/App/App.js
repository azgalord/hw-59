import React, { Component } from 'react';
import LogoContainer from "../../components/LogoContainer/LogoContainer";
import JokeContainer from "../../components/JokeContainer/JokeContainer";

import './App.css';

class App extends Component {

  state = {
    img: '',
    joke: ''
  };

  getRequest = () => {
    fetch('https://api.chucknorris.io/jokes/random')
      .then((result) => {
        if (result.ok) {
          return result.json();
        }

        throw new Error('Something wrong with network request');
      })
      .then(joke => {
        this.setState({img: joke.icon_url, joke: joke.value})
      })
  };

  componentDidMount() {
    this.getRequest();
  }

  render() {
    return (
      <div className="App">
        <LogoContainer/>
        <JokeContainer
          img={this.state.img}
          text={this.state.joke}
          onClick={this.getRequest}
        />
      </div>
    );
  }
}

export default App;
