import React from 'react';
import './WatchList.css';
import MovieItem from "./MovieItem/MovieItem";

const WatchList = props => {
  return (
    <div className="WatchList">
      <h2>To watch list:</h2>
      {props.movieItems.map((item, index) => (
        <MovieItem
          value={item.name}
          onChange={(event) => props.onChange(index, event)}
          onClick={() => props.delete(index)}
          key={index}
        />
      ))}
    </div>
  );
};

export default WatchList;
