import React, {Component} from 'react';
import Chest from '../../../assets/img/times-solid.svg';
import './MovieItem.css';

class MovieItem extends Component {

  shouldComponentUpdate(nextProps) {
    return nextProps.value !== this.props.value;
  }

  render() {
    return (
      <div className="MovieItem">
        <input
          type="text"
          className="MovieItemInput"
          value={this.props.value}
          onChange={this.props.onChange}
        />
        <button
          className="MovieCloseBtn"
          onClick={this.props.onClick}
        >
          <img src={Chest} alt=""/>
        </button>
      </div>
    );
  }
}

export default MovieItem;
