import React from 'react';
import './Input.css';

const Input = props => {
  return (
    <input
      className="InputMain"
      placeholder="Type name of film here"
      type="text"
      value={props.value}
      onChange={props.onChange}
    />
  );
};

export default Input;
