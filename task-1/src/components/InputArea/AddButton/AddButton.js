import React from 'react';
import './AddButton.css';

const AddButton = props => {
  return (
    <button
      className="AddButton"
      onClick={props.onClick}
    >
      Add
    </button>
  );
};

export default AddButton;
