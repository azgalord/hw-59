import React from 'react';
import Input from "./Input/Input";
import AddButton from "./AddButton/AddButton";
import './InputArea.css';

const InputArea = props => {
  return (
    <div className="InputArea">
      <Input
        value={props.inputValue}
        onChange={(event) => props.onChange(event)}
      />
      <AddButton
        onClick={props.onClick()}
      />
    </div>
  );
};

export default InputArea;
