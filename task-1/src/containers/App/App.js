import React, { Component } from 'react';
import InputArea from "../../components/InputArea/InputArea";
import WatchList from "../../components/WatchList/WatchList";

import './App.css';

class App extends Component {
  state = {
    movieItems: [],
    inputValue: ''
  };

  componentDidMount() {
    const movieItems = JSON.parse(localStorage.getItem(0));
    if (movieItems) {
      this.setState({movieItems});
    }
  }

  changeLocalStorage = (movieItems) => {
    const myJson = JSON.stringify(movieItems);
    localStorage.setItem(0, myJson);
  };

  changeInputValue = (event) => {
    const inputValue = event.target.value;
    this.setState({inputValue});
  };

  addItem = () => {
    if (this.state.inputValue !== '') {
      const movieItems = [...this.state.movieItems];
      const item = {name: this.state.inputValue};
      movieItems.push(item);
      this.changeLocalStorage(movieItems);

      this.setState({movieItems, inputValue: ''});
    } else {
      alert('Input area is empty!');
    }
  };

  changeMovieValue = (index, event) => {
    const movieItems = [...this.state.movieItems];
    const movieItem = {...movieItems[index]};
    movieItem.name = event.target.value;
    movieItems[index] = movieItem;
    this.changeLocalStorage(movieItems);

    this.setState({movieItems});
  };

  deleteMovie = (index) => {
    const movieItems = [...this.state.movieItems];
    movieItems.splice(index, 1);
    this.changeLocalStorage(movieItems);

    this.setState({movieItems});
  };

  render() {
    return (
      <div className="App">
        <InputArea
          inputValue={this.state.inputValue}
          onChange={this.changeInputValue}
          onClick={() => this.addItem}
        />
        <WatchList
          movieItems={this.state.movieItems}
          onChange={this.changeMovieValue}
          delete={this.deleteMovie}
        />
      </div>
    );
  }
}

export default App;
